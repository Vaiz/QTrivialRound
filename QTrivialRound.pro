QT += sql widgets
TARGET = QTrivialRound
TEMPLATE = lib
CONFIG += c++11

DEFINES += QTRIVIALROUND_LIBRARY

INCLUDEPATH += $$PWD/src

SOURCES += \
    src/QJson/qjsonfile.cpp \
    src/QLogger/qlogger.cpp \
    src/QLogger/qloggerrecord.cpp \
    src/widgets/qswitchbutton.cpp

HEADERS += \
    src/qtrivialround_global.h \
    src/QJson/qjsonfile.h \
    src/QLogger/qlogger.h \
    src/QLogger/qlogger_p.h \
    src/QLogger/qloggerrecord.h \
    src/QLogger/qloggerrecord_p.h \
    src/widgets/qswitchbutton.h \
    src/widgets/qswitchbutton_p.h

unix: TMP_DIR = .tmp
win32: TMP_DIR = tmp

android {
    CONFIG += staticlib
    DESTDIR = $$PWD/android
    OBJECTS_DIR = $$PWD/$$TMP_DIR/android
    MOC_DIR = $$PWD/$$TMP_DIR/android
    RCC_DIR = $$PWD/$$TMP_DIR/android
    UI_DIR = $$PWD/$$TMP_DIR/android
}
else {
    CONFIG(debug, debug|release) {
        TARGET = QTrivialRound_debug
        DESTDIR = $$PWD/debug
        OBJECTS_DIR = $$PWD/$$TMP_DIR/debug
        MOC_DIR = $$PWD/$$TMP_DIR/debug
        RCC_DIR = $$PWD/$$TMP_DIR/debug
        UI_DIR = $$PWD/$$TMP_DIR/debug
    }
    else {
        DESTDIR = $$PWD/release
        OBJECTS_DIR = $$PWD/$$TMP_DIR/release
        MOC_DIR = $$PWD/$$TMP_DIR/release
        RCC_DIR = $$PWD/$$TMP_DIR/release
        UI_DIR = $$PWD/$$TMP_DIR/release
    }
}

DISTFILES += README.md
