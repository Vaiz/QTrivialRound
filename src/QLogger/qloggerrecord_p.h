#ifndef QLOGGERRECORD_P
#define QLOGGERRECORD_P

#include "qloggerrecord.h"

#include <QDateTime>
#include <QMap>

class QLoggerRecordPrivate
{
    Q_DECLARE_PUBLIC(QLoggerRecord)

public:
    QLoggerRecordPrivate(QLoggerRecord *q);
    QLoggerRecordPrivate &operator =(const QLoggerRecordPrivate &);
    virtual ~QLoggerRecordPrivate();

    QDateTime dateTime;
    QtMsgType msgType;
    QString msg;
    QMap< QString, QString > fields;

    QLoggerRecord * const q_ptr;
};

#endif // QLOGGERRECORD_P

