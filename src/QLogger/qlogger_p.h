#ifndef QLOGGER_P
#define QLOGGER_P

#include "qlogger.h"

#include <QTimer>
#include <QMutex>
#include <QSqlDatabase>

#include "qloggerrecord.h"

class QLoggerPrivate
{
    Q_DECLARE_PUBLIC(QLogger)

public:
    QLoggerPrivate(QLogger *q);

    QString dateTimeFormat;

    QTimer timer;
    int waitTime;
    QList< QLoggerRecord > recordQueue;
    QMutex recordQueueMutex;

    QSqlDatabase db;
    bool dbEnabled;
    QStringList dbFields;

    QFile file;
    bool fileEnabled;
    QString fileName;
    QDate fileDate;
    bool fileAddDate;

    QLogger * const q_ptr;
};

#endif // QLOGGER_P

