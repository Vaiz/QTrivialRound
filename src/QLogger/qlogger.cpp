#include "qlogger.h"
#include "qlogger_p.h"

#include <iostream>

#include <QCoreApplication>
#include <QDateTime>
#include <QDebug>

#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlRecord>

#include <QtConcurrent/QtConcurrent>

using namespace std;

ostream& operator<<(ostream &os, const QString &str)
{
#ifdef Q_OS_WIN
    static QTextCodec *codec = QTextCodec::codecForLocale();
    os << codec->fromUnicode(str).data();
#else
    os << str.toStdString();
#endif
    return os;
}

QLogger &qLogger()
{
    static QLogger qLogger;
    return qLogger;
}

QLoggerPrivate::QLoggerPrivate(QLogger *q)
    : dateTimeFormat("yyyy-MM-dd HH:mm:ss.zzz"),
      waitTime(5000),
      db(QSqlDatabase::addDatabase("QSQLITE", "QLogger")),
      dbEnabled(false),
      fileEnabled(false),
      fileAddDate(false),
      q_ptr(q)
{
}

QLogger::QLogger()
    : QObject(0),
      d_ptr(new QLoggerPrivate(this))
{
}

QLogger::~QLogger()
{
    Q_D(QLogger);

    close();
    if(d->fileEnabled)
        if(d->file.isOpen())
            d->file.close();
    if(d->dbEnabled)
        if(d->db.isOpen())
            d->db.close();

    delete d_ptr;
}

bool QLogger::isOpen()
{
    Q_D(const QLogger);
    return(d->timer.isActive());
}

void QLogger::open()
{
    if(isOpen())
        return;

    Q_D(QLogger);

    connect(&d->timer, SIGNAL(timeout()),
            this, SLOT(addMsgThread()),
            Qt::DirectConnection);

    d->timer.setSingleShot(false);
    d->timer.setInterval(d->waitTime);
    d->timer.start();
}

void QLogger::close()
{
    if(!isOpen())
        return;

    Q_D(QLogger);

    disconnect(&d->timer, SIGNAL(timeout()),
               this, SLOT(addMsgThread()));

    d->timer.stop();
    d->recordQueue.clear();
}

bool QLogger::openFile()
{
    Q_D(QLogger);

    Q_ASSERT(d->fileEnabled);

    if(d->file.isOpen())
        d->file.close();

    QString openFileName = d->fileName;
    if(d->fileAddDate)
    {
        d->fileDate = QDate::currentDate();
        openFileName += d->fileDate.toString(Qt::ISODate);
    }
    openFileName += ".log";

    d->file.setFileName(openFileName);
    if(!d->file.open(QFile::WriteOnly | QIODevice::Append | QIODevice::Text))
    {
        cerr << tr("QLogger: Error opening file %1").arg(openFileName) << endl;
        return false;
    }
    return true;
}

void QLogger::messageHandler(QtMsgType type,
                             const QMessageLogContext &,
                             const QString &msg)
{
    qLogger().addMsg(type, msg);
}

QString QLogger::dateTimeFormat()
{
    Q_D(const QLogger);
    return d->dateTimeFormat;
}

void QLogger::setDateTimeFormat(const QString &dateTimeFormat)
{
    Q_D(QLogger);

    if(d->dateTimeFormat == dateTimeFormat)
        return;

    d->dateTimeFormat = dateTimeFormat;
    emit dateTimeFormatChanged();
}

void QLogger::setFile(const QString &fileName,
                      bool addDate)
{
    QFileInfo fileInfo(fileName);
    QDir dir(fileInfo.absolutePath());
    if(!dir.exists())
    {
        cout << tr("QLogger: Can't open file: path %1 doesn't exist")
                .arg(fileInfo.absolutePath())
             << endl;
        return;
    }

    Q_D(QLogger);
    d->fileEnabled = true;
    d->fileAddDate = addDate;
    d->fileName = fileName;
    openFile();
}

void QLogger::writeToFile(const QString &date,
                          const QString &msg)
{
    Q_D(QLogger);
    if(!d->fileEnabled)
        return;

    if(d->fileAddDate)
        if(d->fileDate != QDate::currentDate())
            openFile();

    if(d->file.isOpen())
    {
        d->file.write(date.toLocal8Bit());
        d->file.write(": ");
        d->file.write(msg.toLocal8Bit());
        d->file.write("\r\n");
    }
}

void QLogger::addMsg(const QLoggerRecord &record)
{
    Q_D(QLogger);

    emit newMsg(record);

    QString typeStr;
    switch(record.msgType())
    {
    case QtInfoMsg:
        typeStr = "II";
        emit info(record);
        break;

    case QtWarningMsg:
        typeStr = "WW";
        emit warning(record);
        break;

    case QtCriticalMsg:
        typeStr = "EE";
        emit error(record);
        break;

    case QtFatalMsg:
        typeStr = "FF";
        emit fatal(record);
        break;

    case QtDebugMsg:
        typeStr = "DD";
        emit debug(record);
        break;
    }

    QString consoleMsg = QString("(%1) %2")
            .arg(typeStr)
            .arg(record.msg());
    cout << consoleMsg << endl;

    QString date = record.dateTime().toString(dateTimeFormat());
    writeToFile(date, consoleMsg);

    if(d->timer.isActive())
    {
        d->recordQueueMutex.lock();
        d->recordQueue += record;
        d->recordQueueMutex.unlock();
    }
}

void QLogger::addMsg(const QtMsgType type,
                     const QString &msg,
                     const QMap< QString, QString > &dbFields)
{
    QLoggerRecord record;
    record.setMsgType(type);
    record.setMsg(msg);

    if(&dbFields != 0)
        record.setFields(dbFields);

    addMsg(record);
}

void QLogger::addMsgThread()
{
    Q_D(QLogger);
    if(d->recordQueue.isEmpty())
        return;

    QList< QLoggerRecord > records;

    d->recordQueueMutex.lock();
    records.swap(d->recordQueue);
    d->recordQueueMutex.unlock();

    if(d->fileEnabled)
        if(!d->file.flush())
            cerr << tr("QLogger: Error write to file") << endl;
    writeToDb(records);
}

QStringList QLogger::dbFields() const
{
    Q_D(const QLogger);
    return d->dbFields;
}

QSqlDatabase QLogger::db() const
{
    Q_D(const QLogger);
    return d->db;
}

void QLogger::setDb(const QString &path,
                    const QStringList &fields)
{
    QFileInfo fileInfo(path);
    QDir dir(fileInfo.absolutePath());
    if(!dir.exists())
    {
        cout << tr("QLogger: Can't open db: path %1 doesn't exist")
                .arg(fileInfo.absolutePath())
             << endl;
        return;
    }

    Q_D(QLogger);
    d->dbFields = fields;
    if(!openDb(path))
        cerr << tr("QLogger: Can't open db") << endl;
}

bool QLogger::openDb(const QString &path,
                     bool recreateDb)
{
    Q_D(QLogger);
    if(d->db.isOpen())
    {
        cerr << tr("QLogger: Previous BD connection has been closed") << endl;
        d->db.close();
    }

    if(recreateDb)
        QFile::remove(path);

    d->db.setDatabaseName(path);

    if(!d->db.open())
    {
        cerr << tr("QLogger: Db open error: %1").arg(d->db.lastError().text())
             << endl;
        return false;
    }

    QSqlQuery query(d->db);
    query.prepare("SELECT name FROM sqlite_master "
                  "WHERE type='table' AND name='log';");
    if(!query.exec())
    {
        cerr << tr("QLogger: Error on check table exist: %1")
                .arg(query.lastError().text())
             << endl;
        return false;
    }

    bool createTable = false;
    if(!query.next())
        createTable = true;
    else
    {
        query.clear();
        query.prepare("PRAGMA table_info('log')");
        if(!query.exec())
        {
            cerr << tr("QLogger: Error on check table info: %1")
                    .arg(query.lastError().text())
                 << endl;
            return false;
        }

        QVector< QString > existFields;
        while(query.next())
            existFields += query.value("name").toString();

        QStringList needFields;
        needFields << "id" << "date" << "type" << "msg";
        needFields += dbFields();
        for(const QString &field : needFields)
        {
            if(!existFields.contains(field))
            {
                createTable = true;
                break;
            }
        }
    }

    if(createTable)
    {
        cout << tr("QLogger: table 'log' has been created") << endl;
        query.prepare("DROP TABLE IF EXISTS 'log'");
        if(!query.exec())
        {
            cerr << tr("QLogger: Error on drop table: %1")
                    .arg(query.lastError().text())
                 << endl;
            return false;
        }

        QString createTableSql = "CREATE TABLE 'log' ("
                                 "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                                 "date VARCHAR(25), "
                                 "type INTEGER, "
                                 "msg VARCHAR(128)";
        for(const QString &field : dbFields())
            createTableSql += QString(", %1 VARCHAR(128)").arg(field);
        createTableSql += ");";

        if(!query.exec(createTableSql))
        {
            cerr << tr("QLogger: Error on create table: \r\n'%1'\r\nQuery: %2")
                    .arg(query.lastError().text())
                    .arg(query.lastQuery())
                 << endl;
            return false;
        }
    }

    d->dbEnabled = true;
    return true;
}

void QLogger::writeToDb(const QList< QLoggerRecord > &records)
{
    Q_D(const QLogger);

    if(!d->dbEnabled)
        return;

    if(!d->db.isOpen())
        return;

    QString sqlStr = "INSERT INTO `log` (date, type, msg";
    for(const QString key : dbFields())
        sqlStr += ", " + key;
    sqlStr += ")\n"
              "VALUES";

    for(const QLoggerRecord &record : records)
    {
        sqlStr += QString("\n('%1', '%2', '%3'")
                .arg(record.dateTime().toString(dateTimeFormat()))
                .arg(record.msgType())
                .arg(record.msg().replace("'",""));
        for(const QString key : dbFields())
        {
            QString value = record.fields().value(key, "");
            value.replace("'","");
            sqlStr += QString(", '%1'").arg(value);
        }
        sqlStr += "),";
    }
    sqlStr[sqlStr.length() - 1] = ';';

    QSqlQuery query(d->db);
    if(!query.exec(sqlStr))
        cerr << tr("QLogger: Error on insert: %1").arg(query.lastError().text())
             << endl;

    emit bdHasNewRecords();
}

bool QLogger::getLog(QList< QLoggerRecord > &records,
                     const int &count,
                     const QString &filter)
{
    Q_D(const QLogger);

    if(!d->dbEnabled)
    {
        cout << tr("QLogger: Db doesn't set") << endl;
        return false;
    }

    if(!d->db.isOpen())
    {
        cout << tr("QLogger: Db not open") << endl;
        return false;
    }

    QString sql = "SELECT * FROM 'log'\n";

    if(!filter.isEmpty())
        sql += "WHERE " + filter + "\n";

    sql += "ORDER BY `date` DESC\n";
    if(count != -1)
        sql += "LIMIT :cnt";
    sql += ";";

    QSqlQuery query(d->db);
    query.prepare(sql);
    if(count != -1)
        query.bindValue(":cnt", count);

    if(!query.exec())
    {
        cout << tr("QLogger: Can't load log: %1").arg(query.lastError().text())
             << endl;
        cout << "Query: " << query.lastQuery() << endl;
        return false;
    }

    QSqlQueryModel model;
    model.setQuery(query);
    while(model.canFetchMore())
    {
        model.fetchMore();
        QCoreApplication::processEvents();
    }

    for(int i = 0; i < model.rowCount(); ++i)
    {
        QSqlRecord sqlRecord = model.record(i);
        QLoggerRecord record;
        record.setDateTime(QDateTime::fromString(sqlRecord.value("date").toString(),
                                                 dateTimeFormat()));
        record.setMsgType((QtMsgType)sqlRecord.value("msgType").toInt());
        record.setMsg(sqlRecord.value("msg").toString());

        for(const QString &key : dbFields())
            record.insert(key, sqlRecord.value(key).toString());

        records += record;
    }

    return true;
}

void QLogger::recreateDb()
{
    Q_D(const QLogger);
    openDb(d->db.databaseName(), true);
}
