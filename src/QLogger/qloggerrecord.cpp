#include "qloggerrecord.h"
#include "qloggerrecord_p.h"

#include <QMap>
#include <QDateTime>

QLoggerRecordPrivate::QLoggerRecordPrivate(QLoggerRecord *q)
    : dateTime(QDateTime::currentDateTime()),
      msgType(QtDebugMsg),
      msg(""),
      q_ptr(q)
{}

QLoggerRecordPrivate &QLoggerRecordPrivate::operator =(const QLoggerRecordPrivate &other)
{
    if(this == &other)
        return *this;

    dateTime = other.dateTime;
    msgType = other.msgType;
    msg = other.msg;
    fields = other.fields;

    return *this;
}

QLoggerRecordPrivate::~QLoggerRecordPrivate()
{}

QLoggerRecord::QLoggerRecord()
    : d_ptr(new QLoggerRecordPrivate(this))
{}

QLoggerRecord::QLoggerRecord(const QLoggerRecord &other)
    : d_ptr(new QLoggerRecordPrivate(this))
{
    *d_ptr = *other.d_ptr;
}

QLoggerRecord &QLoggerRecord::operator =(const QLoggerRecord &other)
{
    if(this == &other)
        return *this;

    *d_ptr = *other.d_ptr;
    return *this;
}

QLoggerRecord::~QLoggerRecord()
{
    delete d_ptr;
}

QDateTime QLoggerRecord::dateTime() const
{
    Q_D(const QLoggerRecord);
    return d->dateTime;
}

void QLoggerRecord::setDateTime(const QDateTime &dateTime)
{
    Q_D(QLoggerRecord);
    d->dateTime = dateTime;
}

void QLoggerRecord::setCurentDateTime()
{
    setDateTime(QDateTime::currentDateTime());
}

QtMsgType QLoggerRecord::msgType() const
{
    Q_D(const QLoggerRecord);
    return d->msgType;
}

void QLoggerRecord::setMsgType(const QtMsgType &msgType)
{
    Q_D(QLoggerRecord);
    d->msgType = msgType;
}

QString QLoggerRecord::msg() const
{
    Q_D(const QLoggerRecord);
    return d->msg;
}

void QLoggerRecord::setMsg(const QString &msg)
{
    Q_D(QLoggerRecord);
    d->msg = msg;
}

QMap< QString, QString > QLoggerRecord::fields() const
{
    Q_D(const QLoggerRecord);
    return d->fields;
}

void QLoggerRecord::setFields(const QMap< QString, QString > &fields)
{
    Q_D(QLoggerRecord);
    d->fields = fields;
}

QString QLoggerRecord::value(const QString &key,
                             const QString &def) const
{
    Q_D(const QLoggerRecord);
    if(d->fields.contains(key))
        return d->fields[key];
    else
        return def;
}

void QLoggerRecord::insert(const QString &key,
                           const QString &value)
{
    Q_D(QLoggerRecord);
    d->fields.insert(key, value);
}
