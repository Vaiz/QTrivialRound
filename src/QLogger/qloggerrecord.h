#ifndef QLOGGERRECORD_H
#define QLOGGERRECORD_H

#include <QMap>
#include <QDateTime>

#include "qtrivialround_global.h"

class QLoggerRecordPrivate;

class QTRIVIALROUNDSHARED_EXPORT QLoggerRecord
{
public:
    QLoggerRecord();
    QLoggerRecord(const QLoggerRecord &);
    QLoggerRecord &operator =(const QLoggerRecord &);
    virtual ~QLoggerRecord();

    QDateTime dateTime() const;
    void setDateTime(const QDateTime &);
    void setCurentDateTime();

    QtMsgType msgType() const;
    void setMsgType(const QtMsgType &);

    QString msg() const;
    void setMsg(const QString &);

    QMap< QString, QString > fields() const;
    void setFields(const QMap< QString, QString > &);

    QString value(const QString &key,
                  const QString &def = "") const;
    void insert(const QString &key,
                const QString &value);

protected:
    QLoggerRecordPrivate * const d_ptr;

private:
    Q_DECLARE_PRIVATE(QLoggerRecord)
};

#endif // QLOGGERRECORD_H
