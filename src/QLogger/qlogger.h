#ifndef QLOGGER_H
#define QLOGGER_H

#include <QObject>
#include <QFile>
#include <QDate>
#include <QTimer>
#include <QSqlQueryModel>

#include "qtrivialround_global.h"
#include "qloggerrecord.h"

class QLoggerPrivate;

class QTRIVIALROUNDSHARED_EXPORT QLogger : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString dateTimeFormat READ dateTimeFormat
               WRITE setDateTimeFormat NOTIFY dateTimeFormatChanged)

public:
    QLogger();
    virtual ~QLogger();

    static void messageHandler(QtMsgType type,
                               const QMessageLogContext &context,
                               const QString &msg);

    QString dateTimeFormat();
    void setDateTimeFormat(const QString &);

    QSqlDatabase db() const;
    void setDb(const QString &path,
               const QStringList &fields = QStringList());
    QStringList dbFields() const;

    void setFile(const QString &filename,
                 bool addDate = false);

    void addMsg(const QLoggerRecord &);
    void addMsg(const QtMsgType type,
                const QString &msg,
                const QMap< QString, QString > &dbFields = QMap< QString, QString >());
    bool getLog(QList< QLoggerRecord > &records,
                const int &count = 100,
                const QString &filter = "");
    void recreateDb();
    bool isOpen();

public slots:
    void open();
    void close();

signals:
    void dateTimeFormatChanged();

    void info(QLoggerRecord);
    void debug(QLoggerRecord);
    void warning(QLoggerRecord);
    void error(QLoggerRecord);
    void fatal(QLoggerRecord);
    void newMsg(QLoggerRecord);

    void bdHasNewRecords();

protected:
    bool openDb(const QString &path,
                bool recreateDb = false);
    void writeToDb(const QList< QLoggerRecord > &);

    bool openFile();
    void writeToFile(const QString &date,
                     const QString &msg);

private slots:
    void addMsgThread();

protected:
    QLoggerPrivate * const d_ptr;

private:
    Q_DECLARE_PRIVATE(QLogger)
};

QLogger QTRIVIALROUNDSHARED_EXPORT &qLogger();

#endif // QLOGGER_H
