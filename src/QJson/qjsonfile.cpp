#include "qjsonfile.h"

#include <QDebug>
#include <QJsonObject>

QJsonFile::QJsonFile(const QString &filename)
    : QJsonDocument()
{
    _filename = filename;
    QFile file;
    file.setFileName(_filename);
    if(!file.open(QFile::ReadOnly) && file.exists())
    {
        QString msg = "QJsonFile: " + file.errorString();
        qCritical() << msg;
        return;
    }

    QJsonDocument jsonDoc = QJsonDocument::fromJson(file.readAll());
    this->setObject(jsonDoc.object());
    file.close();
}

void QJsonFile::sync() const
{
    QFile file;
    file.setFileName(_filename);
    if(!file.open(QFile::WriteOnly))
    {
        QString msg = "QJsonFile: " + file.errorString();
        qCritical() << msg;
        return;
    }

    if(file.write(this->toJson()) == -1)
    {
        QString msg = "QJsonFile: " + file.errorString();
        qCritical() << msg;
    }
    file.close();
}

QString QJsonFile::fileName() const
{
    return _filename;
}
