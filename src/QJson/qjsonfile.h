#ifndef QJSONFILE_H
#define QJSONFILE_H

#include <QFile>
#include <QJsonDocument>

#include "qtrivialround_global.h"

class QTRIVIALROUNDSHARED_EXPORT QJsonFile : public QJsonDocument
{
public:
    explicit QJsonFile(const QString &filename);
    void sync() const; // записывает изменения в файл

    QString fileName() const;

private:
    QString _filename;
};

#endif // QJSONFILE_H
