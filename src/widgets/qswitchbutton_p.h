#ifndef QSWITCHBUTTON_P
#define QSWITCHBUTTON_P

#include "qswitchbutton.h"

class QSwitchButtonPrivate
{
    Q_DECLARE_PUBLIC(QSwitchButton)

public:
    QSwitchButtonPrivate(QSwitchButton *q);

    bool state;
    bool autoSwitch;
    QIcon offIcon;
    QIcon onIcon;

    QSwitchButton * const q_ptr;
};

#endif // QSWITCHBUTTON_P

