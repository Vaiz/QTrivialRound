#include "qswitchbutton.h"
#include "qswitchbutton_p.h"

QSwitchButtonPrivate::QSwitchButtonPrivate(QSwitchButton *q)
    : state(false),
      autoSwitch(false),
      q_ptr(q)
{

}

QSwitchButton::QSwitchButton(QWidget * parent)
    : QToolButton(parent),
      d_ptr(new QSwitchButtonPrivate(this))
{
    connect(this, SIGNAL(clicked(bool)), this, SLOT(onClick()));
}

QSwitchButton::~QSwitchButton()
{
    delete d_ptr;
}

bool QSwitchButton::state() const
{
    Q_D(const QSwitchButton);
    return d->state;
}

void QSwitchButton::setState(const bool state)
{
    Q_D(QSwitchButton);

    if(d->state == state)
        return;

    d->state = state;
    emit stateChanged(d->state);

    if(d->state)
        setIcon(d->onIcon);
    else
        setIcon(d->offIcon);
}

void QSwitchButton::on()
{
    setState(true);
}

void QSwitchButton::off()
{
    setState(false);
}

bool QSwitchButton::autoSwitch() const
{
    Q_D(const QSwitchButton);
    return d->autoSwitch;
}

void QSwitchButton::setAutoSwitch(const bool autoSwitch)
{
    Q_D(QSwitchButton);

    if(d->autoSwitch == autoSwitch)
        return;

    d->autoSwitch = autoSwitch;
    emit autoSwitchChanged(d->autoSwitch);
}

void QSwitchButton::setOnIcon(const QIcon &icon)
{
    Q_D(QSwitchButton);

    d->onIcon = icon;

    if(d->state)
        setIcon(d->onIcon);
}

void QSwitchButton::setOffIcon(const QIcon &icon)
{
    Q_D(QSwitchButton);

    d->offIcon = icon;

    if(!d->state)
        setIcon(d->offIcon);
}

void QSwitchButton::onClick()
{
    Q_D(const QSwitchButton);

    if(d->state)
        emit offRequest();
    else
        emit onRequest();

    if(d->autoSwitch)
        setState(!d->state);
}
