#ifndef QSWITCHBUTTON_H
#define QSWITCHBUTTON_H

#include <QToolButton>

#include "qtrivialround_global.h"

class QSwitchButtonPrivate;

class QTRIVIALROUNDSHARED_EXPORT QSwitchButton : public QToolButton
{
    Q_OBJECT

    Q_PROPERTY(bool state READ state WRITE setState NOTIFY stateChanged)
    Q_PROPERTY(bool autoSwitch READ autoSwitch WRITE setAutoSwitch NOTIFY autoSwitchChanged)

public:
    QSwitchButton(QWidget * parent = 0);
    virtual ~QSwitchButton();

    bool state() const;
    bool autoSwitch() const;

    void setOnIcon(const QIcon &);
    void setOffIcon(const QIcon &);

public slots:
    void setState(const bool);
    void on();
    void off();
    void setAutoSwitch(const bool);

signals:
    void stateChanged(bool);
    void autoSwitchChanged(bool);
    void onRequest();
    void offRequest();

private slots:
    void onClick();

protected:
    QSwitchButtonPrivate * const d_ptr;

private:
    Q_DECLARE_PRIVATE(QSwitchButton)
};

#endif // QSWITCHBUTTON_H
