#!/bin/bash
cd $(dirname $0)
cd ..
mkdir -p tmp

cd tmp
qmake ../QTrivialRound.pro
make
cd ..
rm -rf tmp

install -D -m644 src/qtrivialround_global.h     include/qtrivialround_global.h
install -D -m644 src/QJson/qjsonfile.h          include/qjsonfile.h
install -D -m644 src/QLogger/qlogger.h          include/qlogger.h
install -D -m644 src/QLogger/qloggerrecord.h    include/qloggerrecord.h
install -D -m644 src/widgets/qswitchbutton.h    include/widgets/qswitchbutton.h
