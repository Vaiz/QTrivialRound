@echo off
echo Add path to 'qmake.exe' and 'mingw32-make.exe' to system variable 'path'
echo (Example ';C:\Qt\Tools\mingw492_32\bin;C:\Qt\5.5\mingw492_32\bin')

cd /d "%~dp0"
cd ..
mkdir tmp
mkdir include
cd include
mkdir widgets
cd ..

cd tmp
qmake ../QTrivialRound.pro
mingw32-make -s

cd ..
rmdir /S /Q tmp

copy "%CD%\src\qtrivialround_global.h" "%CD%\include\qtrivialround_global.h"
copy "%CD%\src\QJson\qjsonfile.h" "%CD%\include\qjsonfile.h"
copy "%CD%\src\QLogger\qlogger.h" "%CD%\include\qlogger.h"
copy "%CD%\src\QLogger\qloggerrecord.h" "%CD%\include\qloggerrecord.h"
copy "%CD%\src\widgets\qswitchbutton.h" "%CD%\include\widgets\qswitchbutton.h"